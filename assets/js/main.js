var joy, socket, lastX, lastY;
var jsctrl = false;

// Constants that will be replaced via the percent strings by a preprocessor
const VID_RES = typeof ("%VID_RES%") == 'number' ? "%VID_RES%" : 4;
const LED = typeof ("%LED%") == 'boolean' ? "%LED%" : false;
const JOYSTICK_INTERVAL = typeof ("%JOYSTICK_INTERVAL%") == 'number' ? "%JOYSTICK_INTERVAL%" : 100;

var externalInputDevices = {
	'keybd': {
		'w': false, 'a': false, 's': false, 'd': false,
		'ArrowUp': false, 'ArrowDown': false, 'ArrowLeft': false, 'ArrowRight': false,
	},
	'gamepads': []
}

function setSliderLabelOnChange(event) {
	let label = event.target.nextElementSibling;
	label.innerText = event.target.value;
}

function sendForm(uri, params) {
	var xhttp = new XMLHttpRequest();
	xhttp.open("POST", uri + "?" + params, true);
	xhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			// Response may contain query string with element IDs and their new values
			var response = this.responseText;
			if (response.length > 0) {
				var params = response.split('&');
				for (var i = 0; i < params.length; i++) {
					var param = params[i].split('=');
					var element = document.getElementById(param[0]);
					if (element) {
						element.value = param[1];
						if (element.attributes.type != undefined && element.attributes.type.nodeValue == "range")
							element.dispatchEvent(new Event('input'));
					}
				}
			}
		}
	};
	xhttp.send(null);
}

function processForm(event) {
	event.preventDefault();
	var params = new URLSearchParams(new FormData(event.target)).toString();
	sendForm("/save", params);
}

function onInputChanged(event) {
	var params = new URLSearchParams();
	if (event.target.attributes.type != undefined && event.target.attributes.type.nodeValue == "checkbox")
		params.append(event.target.id, event.target.checked ? 1 : 0);
	else
		params.append(event.target.id, event.target.value);
	sendForm("/click", params.toString());
}

function sendJoystick() {
	if (socket.readyState == 1) {
		if (joy.GetX() != 0 || joy.GetY() != 0) {
			socket.send('steer|' + joy.GetX() + '|' + joy.GetY());
			lastX = joy.GetX();
			lastY = joy.GetY();
		} else if (lastX != 0 || lastY != 0) {
			socket.send('steer|0|0');
			lastX = 0;
			lastY = 0;
		}
	}
}

window.addEventListener('load', function () {
	joy = new JoyStick('joystick');
	let local = false;
	if (window.location.host === "") {
		local = true;
		console.log('Local testing mode, not connecting to server');
	} else {
		socket = new WebSocket("ws://" + window.location.host + "/ws");

		socket.addEventListener('open', function (event) {
			console.log('connected to server');
			let feed = document.getElementById('feed');
			feed.classList.remove('noconnection');
			feed.classList.add('yesconnection');
		});
		socket.addEventListener('close', function (event) {
			console.log('disconnected from server');
			let feed = document.getElementById('feed');
			feed.classList.remove('yesconnection');
			feed.classList.add('noconnection');
		});
		socket.addEventListener('error', function (event) {
			console.log('Error: ' + event);
		});
	}
	for (let sliderWrapper of document.getElementsByClassName('slider')) {
		let slider = sliderWrapper.getElementsByTagName('input')[0];
		slider.addEventListener('input', setSliderLabelOnChange);
		slider.dispatchEvent(new Event('input'));
	}
	for (let form of document.getElementsByTagName('form')) {
		form.addEventListener('submit', processForm);
	}
	for (let element of document.querySelectorAll("input[type=range],input[type=checkbox],select")) {
		element.addEventListener('change', onInputChanged);
	}
	document.getElementById('video_resolution').value = VID_RES;
	document.getElementById('ledswitch').value = LED;

	if (!local)
		setInterval(sendJoystick, JOYSTICK_INTERVAL);
});

window.addEventListener('keydown', function (event) {
	if (event.key in externalInputDevices.keybd) {
		externalInputDevices.keybd[event.key] = true;
	}
});
window.addEventListener('keyup', function (event) {
	if (event.key in externalInputDevices.keybd) {
		externalInputDevices.keybd[event.key] = false;
	}
});

function setJoystickControl(state) {
	if (jsctrl != state) {
		let target;
		let type;
		if (state) {
			target = document.getElementById('joystick').children[0];
			type = 'mousedown';
		} else {
			target = document;
			type = 'mouseup';
		}
		target.dispatchEvent(new MouseEvent(type));
		jsctrl = state;
	}
}

function weightedSum(inputs, weights) {
	let output = 0
	for (i in weights) {
		output += inputs[i] * weights[i];
	}
	return output;
}

function busyExternalInputHandler() {
	//vertical and horizontal simulated axes
	let vAxis = 0;
	let hAxis = 0;

	//get keyboard inputs via a method similar to the controller mapping further down
	let keybd = externalInputDevices.keybd;
	let inputVector = [keybd.w, keybd.a, keybd.s, keybd.d,
	keybd.ArrowUp, keybd.ArrowDown, keybd.ArrowLeft, keybd.ArrowRight];
	let weightMapping = {
		xs: [0, -1, 0, 1, 0, 0, -1, 1],
		ys: [-1, 0, 1, 0, -1, 1, 0, 0],
	}
	hAxis += weightedSum(inputVector, weightMapping.xs);
	vAxis += weightedSum(inputVector, weightMapping.ys);
	//get gamepad inputs
	for (let gp of navigator.getGamepads()) {
		if (gp !== null && gp.connected) {
			//detecting controller mapping
			// this is a weight array that specifies which axes are meant to go
			// vertical and which are meant to go horizontal. This way, we can
			// also account for inverted axis by writing -1 instead of 1 and
			// axes that don't have a range of -1 to 1 by writing a different
			// value to multiply the axis by. The only thing it doesn't support
			// is axis that don't center on 0, of which hopefully none exist.
			let mapping = { xs: [], ys: [] };

			if (gp.mapping === 'standard') {
				// see https://w3c.github.io/gamepad/#fig-visual-representation-of-a-standard-gamepad-layout
				mapping.xs = [1, 0, 1, 0];
				mapping.ys = [0, 1, 0, 1];
				//abandon all hope, ye who enter nonstandard controller area
			} else if (gp.id.endsWith('X-Box 360 pad')) {
				// left stick (XY), left trigger (WIP), right stick (XY), right trigger (WIP), D-Pad (XY)
				mapping.xs = [1, 0, 0, 1, 0, 0, 1, 0];
				mapping.ys = [0, 1, 0, 0, 1, 0, 0, 1];
			} else if (gp.id.endsWith('PLAYSTATION(R)3 Controller')) {
				// left stick (XY), left trigger (WIP), right stick (XY), right trigger (WIP)
				mapping.xs = [1, 0, 0, 1, 0, 0];
				mapping.ys = [0, 1, 0, 0, 1, 0];
			} else if (gp.id.endsWith('Right Joy-Con')) {
				// single stick (XY)
				mapping.xs = [1, 0];
				mapping.ys = [0, 1];
			} else if (gp.id.endsWith('Left Joy-Con')) {
				// single stick (XY)
				mapping.xs = [1, 0];
				mapping.ys = [0, 1];
			} else {
				if (!window.hasAlertedUnknownGamepad) {
					window.hasAlertedUnknownGamepad = true;
					alert("You've got an unknown gamepad: " + gp.id);
				}
			}

			hAxis += weightedSum(gp.axes, mapping.xs);
			vAxis += weightedSum(gp.axes, mapping.ys);
		}
	}

	//clamp to bounds 0 | 2
	vAxis = Math.max(0, Math.min((0.5 * vAxis + 1), 2));
	hAxis = Math.max(0, Math.min((0.5 * hAxis + 1), 2));

	//get joystick position
	let joycanv = document.getElementById('joystick').children[0];
	let jrect = joycanv.getBoundingClientRect();
	let jx = jrect.left;
	let jy = jrect.top;
	let jw = jrect.width;
	let jh = jrect.height;
	//set joystick
	if (vAxis.toFixed(1) != 1 || hAxis.toFixed(1) != 1) {
		setJoystickControl(true);
		let event = new MouseEvent('mousemove', {
			clientX: jx + (jw * hAxis) / 2,
			clientY: jy + (jh * vAxis) / 2
		});
		document.dispatchEvent(event);
	} else {
		setJoystickControl(false);
	}
}
setInterval(busyExternalInputHandler, 20);
